from flask import Flask
import api, admin
from config import config
import web

app = Flask(__name__)
app.config.update(config)
app.register_blueprint(api.api_blueprint)
app.register_blueprint(admin.admin_blueprint)
app.register_blueprint(web.web_blueprint)

if __name__ == "__main__":
    app.run(use_debugger=False, use_reloader=False, host='0.0.0.0', port=8080)


