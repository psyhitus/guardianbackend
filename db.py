from _sha1 import sha1
import hmac
from uuid import uuid4

__author__ = 'Admin'

from mongoengine import Document, StringField, DateTimeField, IntField, PointField, ReferenceField
import datetime


class User(Document):
    name = StringField(max_length=50)
    password = StringField(max_length=50)
    token = StringField()

    @staticmethod
    def generate_key():
        # Get a random UUID.
        new_uuid = uuid4()
        # Hmac that beast.
        return hmac.new(new_uuid.bytes, digestmod=sha1).hexdigest()

    def save(self, *args, **kwargs):
        self.token = self.generate_key()
        return super(User, self).save(*args, **kwargs)


class Coordinate(Document):
    point = PointField()
    update_date = DateTimeField(default=datetime.datetime.utcnow)
    user = ReferenceField(User)

    def save(self, *args, **kwargs):
        self.update_date = datetime.datetime.utcnow()
        return super(Coordinate, self).save(*args, **kwargs)


class Event(Document):
    message = StringField()
    coordinate = ReferenceField(Coordinate)
    user = ReferenceField(User)


class Zone(Document):
    point = PointField()
    title = StringField()
    radius = IntField()


class BeaconType:
    BLE = 0
    WIFI = 1


class Beacon(Document):
    point = PointField()
    type = IntField(min_value=0, max_value=1, default=BeaconType.BLE)


class AccessType:
    DISALLOW = 0
    ALLOW = 1


class AccessRight(Document):
    zone = ReferenceField(Zone)
    user = ReferenceField(User)
    access_type = IntField(min_value=0, max_value=1, default=AccessType.DISALLOW)