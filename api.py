from flask import request, Blueprint, jsonify
from db import User, Coordinate, Zone, Beacon, AccessRight, Event
from helpers import permit, require, GuardianException, make_json_response_from_list, connect_to_db

__author__ = 'psyhitus'

api_blueprint = Blueprint(name='api', import_name=__name__, url_prefix='/api')


@api_blueprint.route('/login', methods=['POST'])
def login():
    data = request.get_json()

    require(data, ['name', 'password'])
    fields = permit(data, ['name', 'password'])

    name = fields['name']
    password = fields['password']

    connect_to_db()
    user = User.objects(name=name).first()

    if not user:
        # создаем пользователя
        user = User(name=name, password=password)
        user.save()
        user.reload()
        token = user.token
    else:
        # пользователь существует
        # проверяем пароль
        if user.password == fields['password']:
            token = user.token
        else:
            raise GuardianException(message='password is incorrect')

    return jsonify({'token': token})


@api_blueprint.route('/logout', methods=['POST'])
def logout():
    pass


def get_user_by_token(token):
    return User.objects(token=token).first()


@api_blueprint.route('/coords', methods=['POST'])
def coords():
    token = request.headers['token']
    data = request.get_json()
    point = data['point']

    connect_to_db()
    user = get_user_by_token(token)
    Coordinate(point=point, user=user).save()

    return '', 200


@api_blueprint.route('/zones', methods=['GET'])
def zones():
    connect_to_db()
    zones_list = Zone.objects()
    return make_json_response_from_list(zones_list)


@api_blueprint.route('/beacons', methods=['GET'])
def beacons():
    connect_to_db()
    beacons_list = Beacon.objects()
    return make_json_response_from_list(beacons_list)


@api_blueprint.route('/permissions', methods=['GET'])
def permissions():
    connect_to_db()
    token = request.headers['token']
    user = get_user_by_token(token)
    permissions_list = AccessRight.objects(user=user)
    return make_json_response_from_list(permissions_list)


@api_blueprint.route('/messages', methods=['GET'])
def messages():
    connect_to_db()
    token = request.headers['token']
    user = get_user_by_token(token)
    events = Event.objects(user=user)
    return make_json_response_from_list(events)


@api_blueprint.errorhandler(Exception)
def handle_error(error):
    try:
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
    except:
        response = jsonify({'error': str(error)})
        response.status_code = 400

    return response