import re
from bson import json_util
from flask import make_response

__author__ = 'psyhitus'
from mongoengine import connect


def replace_ids_dates(x):
    id_regexp = u"\{\"\$oid\": (.{26})\}"
    date_regexp = u"\{\"\$date\": (.{11,14})\}"
    xx = re.sub(id_regexp, lambda a: a.group(1), x)
    xxx = re.sub(date_regexp, lambda a: a.group(1), xx)
    return xxx


def make_json(q):
    string_dict = q.to_mongo()
    json_data = json_util.dumps(string_dict)
    pretty_json_data = replace_ids_dates(json_data)
    return pretty_json_data


def make_json_response(obj):
    json_data = make_json(obj)
    resp = make_response(json_data, 200)
    resp.headers['Content-Type'] = 'application/json'
    return resp


def make_json_response_from_list(obj):
    obj_list = list(obj)
    obj_list = map(lambda o: make_json(o), obj_list)
    json_data = '[%s]' % ' ,'.join(obj_list)
    resp = make_response(json_data, 200)
    resp.headers['Content-Type'] = 'application/json'
    return resp


def permit(data, fields):
    """
    select fields from data
    """
    return {k: v for k, v in data.items() if k in fields}


def require(data, fields):
    """
    check fields existence
    """
    for k in fields:
        if k not in data:
            raise GuardianException(400, '{0} not specified'.format(k))


class GuardianException(Exception):
    DEFAULT_STATUS_CODE = 400

    def __init__(self, status_code=DEFAULT_STATUS_CODE, message=None):
        super(self).__init__()
        self.message = message
        self.status_code = status_code

    def to_dict(self):
        rv = {'message': self.message}
        return rv

    def __str__(self):
        return self.message


def connect_to_db():
    connect("guardian")
