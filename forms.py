from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired

__author__ = 'psyhitus'

from flask.ext.wtf import Form


class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)