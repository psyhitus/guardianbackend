__author__ = 'Admin'

from flask import Blueprint, render_template, request
from db import Zone, Beacon
from helpers import connect_to_db

admin_blueprint = Blueprint(name='admin', import_name=__name__, url_prefix="/admin")


@admin_blueprint.before_request
def before_request():
    connect_to_db()


@admin_blueprint.route("/beacons", methods=["GET"])
def admin_beacons():
    beacons_list = Beacon.objects()
    content = render_template("beacons.html", beacons_list=beacons_list)
    return render_template("admin_template.html", content=content, title="Beacons")


@admin_blueprint.route("/zones", methods=["GET"])
def admin_zones():
    zones_list = Zone.objects()
    content = render_template("zones.html", zones_list=zones_list)
    return render_template("admin_template.html", content=content, title="Zones")


@admin_blueprint.route("/ajax", methods=["POST"])
def ajax():
    data = request.get_json()
    id2 = data["id"]
    print(id2)
    return "ok"