import os

__author__ = 'psyhitus'

SECRET_KEY = 'you-will-never-guess'

config = dict(
    DEBUG=True,
    APP_ROOT_PATH=os.path.dirname(os.path.realpath(__file__)),
    STATIC_FOLDER='static',
    UPLOAD_FOLDER='static',
    SECRET_KEY='hfiovdjqntpoqweiurb-0q2394boimp0gsui',
    # USERNAME='AmazingNerd',
    # PASSWORD='somanywordsinarow',
    WTF_CSRF_ENABLED=True,
    JSONIFY_PRETTYPRINT_REGULAR=True,
    JSON_SORT_KEYS=True,
    SERVER_URL='http://178.62.239.192:8080'
)
