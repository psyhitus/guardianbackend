from flask import Blueprint, render_template, request, flash
from forms import LoginForm

__author__ = 'psyhitus'

web_blueprint = Blueprint(name='web', import_name=__name__)


@web_blueprint.route('/login', methods=['GET', 'POST'])
def login_page():
    form = LoginForm()

    if form.validate_on_submit():
        flash('Login requested for username="%s", password="%s",remember_me=%s' %
              (form.username.data, form.password.data, str(form.remember_me.data)))

    return render_template('login.html', form=form)