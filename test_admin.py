__author__ = 'Admin'

import unittest
import requests
import random
from db import Beacon, Zone
from helpers import connect_to_db


def get_url(url):
    return "http://127.0.0.1:8080" + url


class AdminTestCase(unittest.TestCase):
    def setUp(self):
        connect_to_db()

    def test_beacons(self):
        Beacon.objects().delete()
        for x in range(40, 50):
            for y in range(40, 50):
                Beacon(point=[x, y]).save()
        r = requests.get(get_url("/admin/beacons"))
        assert r.status_code == 200

    def test_zones(self):
        Zone.objects().delete()
        for i in range(1, 10):
            Zone(title="Zone"+str(i),
                 point=[random.randrange(40, 50, 1), random.randrange(40, 50, 1)],
                 radius=random.randrange(50)
            ).save()
        r = requests.get(get_url("/admin/zones"))
        assert r.status_code == 200

if __name__ == '__main__':
    unittest.main()

